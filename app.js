const express=require("express");
const path=require("path");
const bodyParser=require("body-parser");
const cors=require("cors");
const passport=require("passport");
const passportJwt=require("passport-jwt");
const mongoose=require("mongoose");
const config=require('./config/database');

mongoose.connect();

mongoose.connection.on('connected',()=>{
    console.log('connected database ',config.database);
});

mongoose.connection.on('error',(err)=>{
    console.log('Database '+err);
})

const app=express();

const port=3000;
app.use(cors());

app.use(express.static(path.join(__dirname,'public')));

app.use(bodyParser.json());

app.get('/',(req,res)=>{
 res.send("test test");
})

app.listen(port,()=>{
    console.log("Server run on port ",port);

});